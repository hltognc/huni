package com.huni;

import android.app.Application;
import android.content.Context;



/**
 * Created by hltognc on 9.12.2016.
 */

public class MyApplication extends Application {

    private static Context context;

    public static Context getAppContext() {
        return MyApplication.context;
    }

    @Override
    public void onCreate() {
        super.onCreate();


        context = getApplicationContext();

    }
}
