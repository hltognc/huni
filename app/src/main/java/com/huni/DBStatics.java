package com.huni;

import java.security.PublicKey;

/**
 * Created by hltognc on 5.12.2016.
 */

public class DBStatics {

    public static final String DATABASE_NAME = "HuniLocalDB";
    public static final int DATABASE_VERSION = 1;
    public static final String CREATE_PHOTO_TABLE = "CREATE TABLE `Photo` (\n" +
            "\t`ID`\tINTEGER,\n" +
            "\t`URI`\tTEXT NOT NULL,\n" +
            "\tPRIMARY KEY(`ID`)\n" +
            ");";

    public static final String CREATE_PHOTOATT_TABLE =
            "CREATE TABLE `PhotoAtt` (\n" +
                    "\t`ID`\tINTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
                    "\t`Name`\tTEXT NOT NULL\n" +
                    ");";


    public static final String CREATE_PHOTOATTVALUE_TABLE =
            "CREATE TABLE `PhotoAttValue` (\n" +
                    "\t`AttID`\tINTEGER NOT NULL,\n" +
                    "\t`Value`\tTEXT,\n" +
                    "\t`ID`\tINTEGER NOT NULL,\n" +
                    "\tPRIMARY KEY(`ID`),\n" +
                    "\tFOREIGN KEY(`AttID`) REFERENCES `PhotoAttribute`(`ID`)\n" +
                    ");";


    public static final String CREATE_PERSON_TABLE =
            "CREATE TABLE `Person` (\n" +
                    "\t`ID`\tINTEGER NOT NULL,\n" +
                    "\t`Name`\tTEXT,\n" +
                    "\tPRIMARY KEY(`ID`)\n" +
                    ");";


    public static final String CREATE_PHOTOATTVALUEMAP_TABLE =
            "CREATE TABLE `PhotoAttValueMap` (\n" +
                    "\t`PhotoID`\tINTEGER NOT NULL,\n" +
                    "\t`AttValueID`\tINTEGER NOT NULL,\n" +
                    "\t`Confidence`\tTEXT,\n" +
                    "\t`Photoface`\tTEXT,\n" +
                    "\t`ID`\tINTEGER NOT NULL,\n" +
                    "\t`PersonID`\tINTEGER,\n" +
                    "\tPRIMARY KEY(`ID`),\n" +
                    "\tFOREIGN KEY(`PhotoID`) REFERENCES `Photo`(`ID`),\n" +
                    "\tFOREIGN KEY(`AttValueID`) REFERENCES `PhotoAttValue`(`ID`),\n" +
                    "\tFOREIGN KEY(`PersonID`) REFERENCES `Person`(`ID`)\n" +
                    ");";


    public static final String DATABASE_DROP = "select 'drop table ' || name || ';' from " + DATABASE_NAME + "where type = 'table';";

    //table names
    public static final String PHOTO="Photo";
    public static final String PHOTO_ATT="PhotoAtt";
    public static final String PHOTO_ATT_VALUE="PhotoAttValue";
    public static final String PERSON="Person";
    public static final String PHOTO_ATT_VALUE_MAP="PhotoAttValueMap";


    //table columns

    //Photo
    public static final String PHOTO_PHOTO_ID="ID";
    public static final String PHOTO_URI="URI";

    //PhotoAtt
    public static final String PHOTOATT_ID="ID";
    public static final String PHOTOATT_NAME="Name";

    //PhotoAttValue
    public static final String PHOTOATTVALUE_ATTID="AttID";
    public static final String PHOTOATTVALUE_VALUE="Value";
    public static final String PHOTOATTVALUE_ID="ID";

    //Person
    public static final String PERSON_ID="ID";
    public static final String PERSON_NAME="Name";

    //PhotoAttValueMap
    public static final String PHOTOATTVALUEMAP_PHOTOID="PhotoID";
    public static final String PHOTOATTVALUEMAP_ATTVALUEID="AttValueID";
    public static final String PHOTOATTVALUEMAP_CONFIDENCE="Confidence";
    public static final String PHOTOATTVALUEMAP_PHOTOFACE="Photoface";
    public static final String PHOTOATTVALUEMAP_ID="ID";
    public static final String PHOTOATTVALUEMAP_PERSONID="PersonID";




}
