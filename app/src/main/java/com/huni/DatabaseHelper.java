package com.huni;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.huni.model.ImageModel;

import java.util.ArrayList;

/**
 * Created by hltognc on 5.12.2016.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    public final String TAG=this.getClass().getName();

    public DatabaseHelper(Context context) {
        super(context, DBStatics.DATABASE_NAME, null, DBStatics.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DBStatics.CREATE_PHOTO_TABLE);
        db.execSQL(DBStatics.CREATE_PHOTOATT_TABLE);
        db.execSQL(DBStatics.CREATE_PHOTOATTVALUE_TABLE);
        db.execSQL(DBStatics.CREATE_PERSON_TABLE);
        db.execSQL(DBStatics.CREATE_PHOTOATTVALUEMAP_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DBStatics.DATABASE_DROP);
        onCreate(db);
    }

    public boolean insertPhoto(int id, String URI) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBStatics.PHOTO_PHOTO_ID, id);
        contentValues.put(DBStatics.PHOTO_URI, URI);
        db.insert(DBStatics.PHOTO, null, contentValues);
        return true;
    }

    public boolean insertPhotoAtt(int id, String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBStatics.PHOTOATT_ID, id);
        contentValues.put(DBStatics.PHOTOATT_NAME, name);
        db.insert(DBStatics.PHOTO_ATT, null, contentValues);
        return true;
    }

    public boolean insertPhotoAttValue(int attID, String value, int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBStatics.PHOTOATTVALUE_ATTID, attID);
        contentValues.put(DBStatics.PHOTOATTVALUE_VALUE, value);
        contentValues.put(DBStatics.PHOTOATTVALUE_ID, id);
        db.insert(DBStatics.PHOTO_ATT_VALUE, null, contentValues);

        return true;
    }

    public boolean insertPerson(int id, String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBStatics.PERSON_ID, id);
        contentValues.put(DBStatics.PERSON_NAME, name);
        db.insert(DBStatics.PERSON, null, contentValues);
        return true;
    }

    public boolean insertPhotoAttValueMap(int attValueId, String confidence, int id, int personId, String photoFace, int photoId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBStatics.PHOTOATTVALUEMAP_ATTVALUEID, attValueId);
        contentValues.put(DBStatics.PHOTOATTVALUEMAP_CONFIDENCE, confidence);
        contentValues.put(DBStatics.PHOTOATTVALUEMAP_ID, id);
        contentValues.put(DBStatics.PHOTOATTVALUEMAP_PERSONID, personId);
        contentValues.put(DBStatics.PHOTOATTVALUEMAP_PHOTOFACE, photoFace);
        contentValues.put(DBStatics.PHOTOATTVALUEMAP_PHOTOID, photoId);
        db.insert(DBStatics.PHOTO_ATT_VALUE_MAP, null, contentValues);
        return true;
    }

    public boolean checkPhotoUploadStatusByImageUri(String uri) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select id from " + DBStatics.PHOTO + " where Uri = ? " ,new String[]{uri});

        Log.e(TAG,"res getCount= "+res.getCount());
        if (res.getCount()>0)
            return true;
        return false;
    }

    public int getPhotoIdByImageUri(String uri){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + DBStatics.PHOTO + " where Uri = ? " ,new String[]{uri});
        res.moveToFirst();
        if (res.getCount()>0){
            return res.getInt(res.getColumnIndex(DBStatics.PHOTO_PHOTO_ID));
        }
        else {
            return 0;
        }

    }

    public String getPhotoUriById(int id){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + DBStatics.PHOTO + " where id =  " +id+" ",null);
        res.moveToFirst();
        if (res.getCount()>0){
            return res.getString(res.getColumnIndex(DBStatics.PHOTO_URI));
        }
            return "";
        }

    public ArrayList<Integer> getUserImagesPhotoId(){
        ArrayList<Integer> userImagesPhotoIdArrayList = new ArrayList<Integer>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + DBStatics.PHOTO, null);
        res.moveToFirst();
        while (res.isAfterLast() == false) {
            userImagesPhotoIdArrayList.add(res.getInt(res.getColumnIndex(DBStatics.PHOTO_PHOTO_ID)));
            res.moveToNext();
        }


        return userImagesPhotoIdArrayList;
    }


}
