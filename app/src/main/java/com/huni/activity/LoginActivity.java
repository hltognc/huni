package com.huni.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.huni.GeneralValues;
import com.huni.HuniUtil;
import com.huni.PreferencesHuni;
import com.huni.R;
import com.huni.RestInterfaceController;
import com.huni.model.UserLoginTokenModel;


import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class LoginActivity extends AppCompatActivity {


    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
//    private UserLoginTask mAuthTask = null;

    // UI references.
    private EditText mEmailView;
    private EditText mPasswordView;
    private Button mSignInBtn;
    private ProgressDialog progressDialog;
    View focusView = null;
    private TextView mAccountRegisterTextBtn;

    private RestInterfaceController serviceAPI;
    private UserLoginTokenModel userLoginTokenModel;

    private static String grantType = "password";
    private String usernameStr, passwordStr;
    private String error = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Set up the login form.
        checkUserLogInStatus();
        mEmailView = (EditText) findViewById(R.id.email);

        mAccountRegisterTextBtn = (TextView) findViewById(R.id.account_register_text);

        mAccountRegisterTextBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, AccountRegisterActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
            }
        });

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        mSignInBtn = (Button) findViewById(R.id.email_sign_in_button);
        mSignInBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });


    }

    private void checkUserLogInStatus() {
        if (PreferencesHuni.checkPreferencesWhetherTheValueisExistorNot(GeneralValues.LOGIN_USER_NAME)){
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            LoginActivity.this.finish();
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(intent);
        }
        else
            return;
    }


    private void attemptLogin() {
        boolean mCancel = this.logInValidation();
        if (mCancel) {
            focusView.requestFocus();
        } else {
            logIn();
        }
    }


    private void logIn() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(GeneralValues.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        serviceAPI = retrofit.create(RestInterfaceController.class);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();

        retrofit2.Call<UserLoginTokenModel> call = serviceAPI.userLogIn(grantType, usernameStr, passwordStr);
        call.enqueue(new Callback<UserLoginTokenModel>() {
            @Override
            public void onResponse(retrofit2.Call<UserLoginTokenModel> call, Response<UserLoginTokenModel> response) {
                try {
                    int code = response.code();
                    userLoginTokenModel = new UserLoginTokenModel();
                    if (code == 200 || code == 400) {

                        if (!(response.body() == null)) {
                            userLoginTokenModel.setAccessToken(response.body().getAccessToken());
                            userLoginTokenModel.setExpires(response.body().getExpires());
                            userLoginTokenModel.setExpiresIn(response.body().getExpiresIn());
                            userLoginTokenModel.setIssued(response.body().getIssued());
                            userLoginTokenModel.setTokenType(response.body().getTokenType());
                            userLoginTokenModel.setUserName(response.body().getUserName());

                            PreferencesHuni.setValue(GeneralValues.LOGIN_USER_NAME, userLoginTokenModel.getUserName());
                            PreferencesHuni.setValue(GeneralValues.LOGIN_ACCESS_TOKEN, userLoginTokenModel.getAccessToken());

                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);

                            LoginActivity.this.finish();
                            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                            startActivity(intent);
                        } else {
                            HuniUtil.createAlertDialog(LoginActivity.this, "Username or password is wrong.", false);
                        }

                        progressDialog.cancel();
                    } else {
                        progressDialog.cancel();
                        HuniUtil.createSnackbar(LoginActivity.this, getString(R.string.internet_connection_problem));
                    }
                } catch (Exception e) {
                    progressDialog.cancel();
                    HuniUtil.createSnackbar(LoginActivity.this, getString(R.string.something_going_wrong));
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(retrofit2.Call<UserLoginTokenModel> call, Throwable t) {
                progressDialog.cancel();
                HuniUtil.createSnackbar(LoginActivity.this, getString(R.string.internet_connection_problem));
            }
        });


    }

    private boolean logInValidation() {
        //default values
        boolean cancel = false;
        mEmailView.setError(null);
        ;
        mPasswordView.setError(null);

        // Store values at the time of the sign up attempt.
        usernameStr = mEmailView.getText().toString();
        passwordStr = mPasswordView.getText().toString();

        if (passwordStr.length() < 6) {
            HuniUtil.createSnackbar(this, getString(R.string.password_length_failed));
//            mPasswordView.setError(getString(R.string.password_length_failed));
            focusView = mPasswordView;
            cancel = true;
        }

        if (TextUtils.isEmpty(passwordStr)) {
            HuniUtil.createSnackbar(this, getString(R.string.error_field_required));
//            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        }

        if (TextUtils.isEmpty(usernameStr)) {
            HuniUtil.createSnackbar(this, getString(R.string.error_field_required));
//            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else {
            if (!android.util.Patterns.EMAIL_ADDRESS.matcher(usernameStr).matches()) {
                HuniUtil.createSnackbar(this, getString(R.string.error_invalid_email));
//                mEmailView.setError(getString(R.string.error_invalid_email));
                focusView = mEmailView;
                cancel = true;
            }
        }


        return cancel;
    }


}

