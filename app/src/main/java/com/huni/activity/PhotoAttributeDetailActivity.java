package com.huni.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.huni.R;
import com.huni.adapter.PhotoAttributeAdapter;
import com.huni.model.DataAttModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hltognc on 18.12.2016.
 */

public class PhotoAttributeDetailActivity extends AppCompatActivity {

    private List<DataAttModel> dataAttModelArrayList = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private PhotoAttributeAdapter photoAttributeAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_attribute_detail);
        mRecyclerView= (RecyclerView) findViewById(R.id.recyclerView);

        try {
            dataAttModelArrayList = getIntent().getParcelableArrayListExtra("images_attribute");
            RecyclerView.LayoutManager linearLayoutManager=new LinearLayoutManager(PhotoAttributeDetailActivity.this);
            photoAttributeAdapter=new PhotoAttributeAdapter(this,dataAttModelArrayList);
            mRecyclerView.setAdapter(photoAttributeAdapter);
            mRecyclerView.setLayoutManager(linearLayoutManager);
        } catch (Exception e) {

        }
    }
}
