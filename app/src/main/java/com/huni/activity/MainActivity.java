package com.huni.activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.huni.RestInterfaceController;
import com.huni.fragment.GalleryFragment;
import com.huni.GeneralValues;
import com.huni.PreferencesHuni;
import com.huni.R;
import com.huni.fragment.HelpFragment;
import com.huni.fragment.ProfileFragment;
import com.huni.fragment.SendFeedbackFragment;
import com.huni.fragment.SettingsFragment;
import com.huni.model.UserInfoModel;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, SearchView.OnQueryTextListener {

    private TextView navHeaderEmailText, navHeaderUserNameSurname;
    private CircleImageView navHeaderProfileImage;
    private LinearLayout navHeaderCoverPhotoLayout;

    private RestInterfaceController serviceAPI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);
        navHeaderEmailText = (TextView) headerView.findViewById(R.id.nav_header_user_email_text);
        navHeaderUserNameSurname = (TextView) headerView.findViewById(R.id.nav_header_user_name_text);
        navHeaderProfileImage = (CircleImageView) headerView.findViewById(R.id.profile_image);
        navHeaderCoverPhotoLayout = (LinearLayout) headerView.findViewById(R.id.cover_photo);
        setHeaderCurrentValues();
        GalleryFragment galleryFragment = new GalleryFragment();
        FragmentManager manager = getSupportFragmentManager();
        manager.beginTransaction().replace(R.id.content_main,
                galleryFragment).commit();
        getSupportActionBar().setTitle("Gallery");


    }

    public void setHeaderCurrentValues() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(GeneralValues.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        serviceAPI = retrofit.create(RestInterfaceController.class);
        retrofit2.Call<UserInfoModel> call = serviceAPI.getUserInfo("Bearer " + PreferencesHuni.getValue(GeneralValues.LOGIN_ACCESS_TOKEN));
        call.enqueue(new Callback<UserInfoModel>() {
            @Override
            public void onResponse(Call<UserInfoModel> call, Response<UserInfoModel> response) {

                if (response.code() == 200) {


                    navHeaderEmailText.setText(response.body().getEmail());
                    navHeaderUserNameSurname.setText(response.body().getFirstName() + " " + response.body().getLastName());

                    GlideUrl glideUrl = new GlideUrl("http://huni.muhammedtahaikiz.com/api/Photo/GetPhotoByID?photoId=" + response.body().getProfilePhotoID()
                            , new LazyHeaders.Builder()
                            .addHeader("Authorization", "Bearer " + PreferencesHuni.getValue(GeneralValues.LOGIN_ACCESS_TOKEN))
                            .build());
                    Glide.with(MainActivity.this)
                            .load(glideUrl)
                            .placeholder(R.drawable.profile_pic_place_holder)
                            .dontAnimate()
                            .into(navHeaderProfileImage);

                    GlideUrl glideUrl2 = new GlideUrl("http://huni.muhammedtahaikiz.com/api/Photo/GetPhotoByID?photoId=" + response.body().getCoverPhotoID()
                            , new LazyHeaders.Builder()
                            .addHeader("Authorization", "Bearer " + PreferencesHuni.getValue(GeneralValues.LOGIN_ACCESS_TOKEN))
                            .build());
                    Glide.with(MainActivity.this)
                            .load(glideUrl2)
                            .asBitmap()
                            .placeholder(R.drawable.side_nav_bar)
                            .dontAnimate()
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                    Drawable drawable = new BitmapDrawable(resource);
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                        navHeaderCoverPhotoLayout.setBackground(drawable);
                                    }
                                }
                            });


                }

            }

            @Override
            public void onFailure(Call<UserInfoModel> call, Throwable t) {

            }
        });

    }

    @Override
    public void onBackPressed() {
        int count = getFragmentManager().getBackStackEntryCount();
        if (count == 0) {
            super.onBackPressed();
            //additional code
        } else {
            getFragmentManager().popBackStack();
        }
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
//            super.onBackPressed();
//        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //  getMenuInflater().inflate(R.menu.main, menu);
//        getMenuInflater().inflate(R.menu.main,menu);
//        MenuItem searchItem=menu.findItem(R.id.action_search);
//        SearchManager searchManager= (SearchManager) getSystemService(Context.SEARCH_SERVICE);
//        SearchView searchView= (SearchView) MenuItemCompat.getActionView(searchItem);
//        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
//        searchView.setSubmitButtonEnabled(true);
//        searchView.setOnQueryTextListener(this);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_gallery) {
            GalleryFragment galleryFragment = new GalleryFragment();
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(R.id.content_main,
                    galleryFragment).commit();
            getSupportActionBar().setTitle("Gallery");

        } else if (id == R.id.nav_profile) {
            ProfileFragment profileFragment = new ProfileFragment();
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(R.id.content_main,
                    profileFragment).addToBackStack("profile").commit();
            getSupportActionBar().setTitle("Profile");
        } else if (id == R.id.nav_settings) {
            SettingsFragment settingsFragment = new SettingsFragment();
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(R.id.content_main,
                    settingsFragment).commit();
            getSupportActionBar().setTitle("Settings");

        } else if (id == R.id.nav_sendFeedBack) {
            SendFeedbackFragment sendFeedbackFragment = new SendFeedbackFragment();
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(R.id.content_main,
                    sendFeedbackFragment).commit();
            getSupportActionBar().setTitle("SendFeedBack");

        } else if (id == R.id.nav_faq) {
            HelpFragment helpFragment = new HelpFragment();
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(R.id.content_main,
                    helpFragment).addToBackStack("help").commit();
            getSupportActionBar().setTitle("Help");

        } else if (id == R.id.nav_logOut) {
            PreferencesHuni.removeValue(GeneralValues.LOGIN_USER_NAME);
            PreferencesHuni.removeValue(GeneralValues.LOGIN_ACCESS_TOKEN);
            PreferencesHuni.removeValue(GeneralValues.SETTINGS_USE_ONLY_WIFI);
            Intent intent = new Intent(this, LoginActivity.class);
            this.finish();
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }


}
