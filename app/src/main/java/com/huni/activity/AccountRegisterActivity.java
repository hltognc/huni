package com.huni.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ActionMenuView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import com.huni.GeneralValues;
import com.huni.HuniUtil;
import com.huni.R;
import com.huni.RestInterfaceController;


/**
 * Created by hltognc on 21.11.2016.
 */

public class AccountRegisterActivity extends AppCompatActivity {

    //UI references
    private EditText mUserEmail, mUserPassword, mUserConfirmationPassword;
    private Button mSubmitButton;
    private ProgressDialog progressDialog;
    private ActionMenuView amvMenu;
    View focusView = null;
    private RestInterfaceController serviceAPI;
    private String UserEmailStr, UserPasswordStr, UserConfirmationPasswordStr;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_register);



        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        amvMenu = (ActionMenuView) toolbar.findViewById(R.id.amvMenu);

        amvMenu.setOnMenuItemClickListener(new ActionMenuView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return onOptionsItemSelected(item);
            }
        });
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mUserEmail = (EditText) findViewById(R.id.UserEmail);
        mUserPassword = (EditText) findViewById(R.id.UserPassword);
        mUserConfirmationPassword = (EditText) findViewById(R.id.UserPasswordConfirmation);
        mSubmitButton = (Button) findViewById(R.id.submitBtn);


        mSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptSingUp();
            }


        });


    }

    private void attemptSingUp() {
        boolean mCancel=this.signUpValidation();
        if (mCancel){
          focusView.requestFocus();
        }
        else {
            signUp();
        }
    }

    private void signUp() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(GeneralValues.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        serviceAPI = retrofit.create(RestInterfaceController.class);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();

        Call<Void> call = serviceAPI.userSignUp(UserEmailStr, UserPasswordStr,UserConfirmationPasswordStr);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                try{
                    int code=response.code();
                    if (code==200){
                        HuniUtil.createAlertDialog(AccountRegisterActivity.this,getString(R.string.successfully_registration_message),true);
                    }
                    progressDialog.cancel();
                }
                catch (Exception e){
                    progressDialog.cancel();
                    //   HuniUtil.createSnackbar(AccountRegisterActivity.this,getString(R.string.internet_connection_problem));
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                progressDialog.cancel();
                HuniUtil.createSnackbar(AccountRegisterActivity.this,getString(R.string.internet_connection_problem));
            }
        });




    }

    private boolean signUpValidation(){
        //default values
        boolean cancel=false;
        mUserEmail.setError(null);
        mUserPassword.setError(null);
        mUserConfirmationPassword.setError(null);

        // Store values at the time of the sign up attempt.
        UserEmailStr=mUserEmail.getText().toString();
        UserPasswordStr=mUserPassword.getText().toString();
        UserConfirmationPasswordStr=mUserConfirmationPassword.getText().toString();

        if (UserConfirmationPasswordStr.length()<6){
            mUserConfirmationPassword.setError(getString(R.string.password_length_failed));
            focusView=mUserConfirmationPassword;
            cancel=true;
        }
        if (UserPasswordStr.length()<6){
            mUserPassword.setError(getString(R.string.password_length_failed));
            focusView=mUserPassword;
            cancel=true;
        }

        if (TextUtils.isEmpty(UserConfirmationPasswordStr)){
            mUserConfirmationPassword.setError(getString(R.string.error_field_required));
            focusView=mUserConfirmationPassword;
            cancel=true;
        }

        if (TextUtils.isEmpty(UserPasswordStr)){
            mUserPassword.setError(getString(R.string.error_field_required));
            focusView=mUserPassword;
            cancel=true;
        }

        if (!UserPasswordStr.equals(UserConfirmationPasswordStr)){
            mUserConfirmationPassword.setError(getString(R.string.password_confirmation_failed));
            focusView=mUserConfirmationPassword;
            cancel=true;
        }



        if (TextUtils.isEmpty(UserEmailStr)){
            mUserEmail.setError(getString(R.string.error_field_required));
            focusView=mUserEmail;
            cancel=true;
        }
        else {
            if (!android.util.Patterns.EMAIL_ADDRESS.matcher(UserEmailStr).matches()){
                mUserEmail.setError(getString(R.string.error_invalid_email));
                focusView=mUserEmail;
                cancel=true;
            }
        }



        return cancel;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.registration_menu, amvMenu.getMenu());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.backBtn:
                Intent intent=new Intent(AccountRegisterActivity.this,LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                break;
        }
        return true;
    }
}
