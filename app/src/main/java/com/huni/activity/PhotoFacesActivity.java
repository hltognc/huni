package com.huni.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.huni.GeneralValues;
import com.huni.HuniUtil;
import com.huni.PreferencesHuni;
import com.huni.R;
import com.huni.RestInterfaceController;
import com.huni.adapter.PhotoAttributeAdapter;
import com.huni.adapter.PhotoFacesAdapter;
import com.huni.model.PhotoFacesDataModel;
import com.huni.model.PhotoFacesModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by hltognc on 18.12.2016.
 */

public class PhotoFacesActivity extends AppCompatActivity {

    private final String TAG = this.getClass().getName();
    private RecyclerView mRecyclerView;
    private RestInterfaceController serviceAPI;
    private PhotoFacesModel photoFacesModel;
    private ProgressDialog progressDialog;
    private List<PhotoFacesDataModel> photoFacesDataModelArrayList=new ArrayList<>();
    private PhotoFacesAdapter photoFacesAdapter;


    private int photoId;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_faces);
        try {
            photoId = getIntent().getIntExtra("photo_id", 0);
            if (photoId != 0) {


                mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
                checkPhotoFaces(photoId);
            }
        } catch (Exception e) {
            HuniUtil.createSnackbar(this, getResources().getString(R.string.internet_connection_problem));
            Log.e(TAG, " PhotoId catch();");
        }


    }

    private void checkPhotoFaces(int photoId) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(GeneralValues.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        serviceAPI = retrofit.create(RestInterfaceController.class);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();

        retrofit2.Call<PhotoFacesModel> call = serviceAPI.getPhotoFacesByPhotoID("Bearer " + PreferencesHuni.getValue(GeneralValues.LOGIN_ACCESS_TOKEN),
                photoId);
        call.enqueue(new Callback<PhotoFacesModel>() {
            @Override
            public void onResponse(Call<PhotoFacesModel> call, Response<PhotoFacesModel> response) {
                try {
                    photoFacesDataModelArrayList=new ArrayList<PhotoFacesDataModel>();
                    photoFacesModel = new PhotoFacesModel();
                    if (response.body().getResult() == 1) {
                        photoFacesModel.setData(response.body().getData());
                        photoFacesDataModelArrayList=photoFacesModel.getData();
                        RecyclerView.LayoutManager linearLayoutManager=new LinearLayoutManager(PhotoFacesActivity.this);
                        photoFacesAdapter=new PhotoFacesAdapter(PhotoFacesActivity.this,photoFacesDataModelArrayList);
                        mRecyclerView.setAdapter(photoFacesAdapter);
                        mRecyclerView.setLayoutManager(linearLayoutManager);
                    } else {
                        HuniUtil.createSnackbar(PhotoFacesActivity.this, getString(R.string.internet_connection_problem));

                    }
                    progressDialog.cancel();
                } catch (Exception e) {
                    HuniUtil.createSnackbar(PhotoFacesActivity.this, getString(R.string.internet_connection_problem));
                    progressDialog.cancel();
                    Log.e(TAG, " PhotoFacesModel onResponse Catch();");
                }


            }

            @Override
            public void onFailure(Call<PhotoFacesModel> call, Throwable t) {
                photoFacesDataModelArrayList=new ArrayList<PhotoFacesDataModel>();
                progressDialog.cancel();
                HuniUtil.createSnackbar(PhotoFacesActivity.this, getString(R.string.internet_connection_problem));
            }
        });


    }


}
