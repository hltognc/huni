package com.huni.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.huni.model.ImageModel;
import com.huni.R;

import java.util.ArrayList;

/**
 * Created by hltognc on 30.11.2016.
 */
public class MyViewPagerAdapter extends PagerAdapter {

    private Context context;
    private ArrayList<ImageModel> images;
    private LayoutInflater layoutInflater;

    public MyViewPagerAdapter(Context context, ArrayList<ImageModel> images){
        this.context=context;
        this.images=new ArrayList<>(images);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        layoutInflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view=layoutInflater.inflate(R.layout.image_fullscreen_preview,container,false);

        ImageView imageViewPreview= (ImageView) view.findViewById(R.id.imagePreview);
        ImageModel imageModel=images.get(position);
        Glide.with(context).load(imageModel.getImageUri())
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageViewPreview);

        container.addView(view);

        return view;
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view==(View)object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View)object);
    }
}
