package com.huni.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.huni.R;

import java.util.HashMap;
import java.util.List;

/**
 * Created by hltognc on 10.01.2017.
 */

public class ExpandableListCustomAdapter extends BaseExpandableListAdapter {
    private LayoutInflater layoutInflater;
    private Context context;
    private List<String> listDataHeader;
    private HashMap<String, List<String>> listDataChild;

    public ExpandableListCustomAdapter(Context context, List<String> listDataHeader,
                                       HashMap<String, List<String>> listDataChild) {
        this.context=context;
        layoutInflater=layoutInflater.from(context);
        this.listDataHeader=listDataHeader;
        this.listDataChild=listDataChild;
    }

    @Override
    public int getGroupCount() {
        return listDataHeader.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return listDataChild.get(listDataHeader.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return listDataHeader.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String headerText= (String) getGroup(groupPosition);
        if (convertView==null){
            convertView=layoutInflater.inflate(R.layout.row_help_group,null);
        }
        TextView listHeaderText= (TextView) convertView.findViewById(R.id.listHeader);
        listHeaderText.setText(headerText);

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        String childText=(String)getChild(groupPosition,childPosition);
        if (convertView==null){
            convertView=layoutInflater.inflate(R.layout.row_help_item,parent,false);
        }
        TextView listChildText= (TextView) convertView.findViewById(R.id.listChild);
        listChildText.setText(childText);

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return false;
    }

    @Override
    public void onGroupCollapsed(int groupPosition) {
        super.onGroupCollapsed(groupPosition);
    }

    @Override
    public void onGroupExpanded(int groupPosition) {
        super.onGroupExpanded(groupPosition);
    }
}
