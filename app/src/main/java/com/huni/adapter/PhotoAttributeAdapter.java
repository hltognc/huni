package com.huni.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.huni.model.DataAttModel;
import com.huni.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hltognc on 17.12.2016.
 */

public class PhotoAttributeAdapter extends RecyclerView.Adapter<PhotoAttributeAdapter.MyViewHolder> {
    private LayoutInflater layoutInflater;
    List<DataAttModel> photoAttributeDataList;
    private Context context;

    public PhotoAttributeAdapter(Context context, List<DataAttModel> photoAttributeDataList) {
        this.context = context;
        this.photoAttributeDataList = new ArrayList<>(photoAttributeDataList);
        this.layoutInflater = LayoutInflater.from(context);

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.row_attribute, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final DataAttModel dataAttModel = photoAttributeDataList.get(position);
        holder.bind(dataAttModel);
    }

    @Override
    public int getItemCount() {
        return photoAttributeDataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView attributeNameTxt, attributeValueTxt, confidenceScoreTxt;

        public MyViewHolder(View itemView) {
            super(itemView);
            attributeNameTxt = (TextView) itemView.findViewById(R.id.attributeName);
            attributeValueTxt = (TextView) itemView.findViewById(R.id.attributeValue);
            confidenceScoreTxt = (TextView) itemView.findViewById(R.id.confidenceScore);

        }

        public void bind(DataAttModel dataAttModel) {
            attributeNameTxt.setText(dataAttModel.getAttributeName());
            attributeValueTxt.setText(dataAttModel.getAttributeValue());
            confidenceScoreTxt.setText(String.valueOf(dataAttModel.getConfidenceScore()));
        }
    }
}
