package com.huni.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaderFactory;
import com.bumptech.glide.load.model.LazyHeaders;
import com.huni.GeneralValues;
import com.huni.PreferencesHuni;
import com.huni.R;
import com.huni.RestInterfaceController;
import com.huni.model.PhotoFacesDataModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hltognc on 6.01.2017.
 */

public class PhotoFacesAdapter extends RecyclerView.Adapter<PhotoFacesAdapter.MyViewHolder> {
    private LayoutInflater layoutInflater;
    List<PhotoFacesDataModel> photoFacesDataModelList;
    private Context context;

    public PhotoFacesAdapter(Context context,List<PhotoFacesDataModel> photoFacesDataModelList){
        this.context=context;
        this.photoFacesDataModelList=new ArrayList<>(photoFacesDataModelList);
        this.layoutInflater=LayoutInflater.from(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=layoutInflater.inflate(R.layout.row_photo_faces,parent,false);
        MyViewHolder holder=new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final PhotoFacesDataModel photoFacesDataModel=photoFacesDataModelList.get(position);
        holder.bind(photoFacesDataModel);

    }

    @Override
    public int getItemCount() {
        return photoFacesDataModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView photoFace,photoFacesEmotion;
        private TextView photoFacesName;
        private RestInterfaceController serviceAPI;
        private Boolean emotionBool;
        private Button submitBtn;
        public MyViewHolder(View itemView) {
            super(itemView);
            photoFace= (ImageView) itemView.findViewById(R.id.PhotoFace);
            photoFacesEmotion= (ImageView) itemView.findViewById(R.id.PhotoFacesEmotion);
            submitBtn= (Button) itemView.findViewById(R.id.submitBtn);
            photoFacesName= (TextView) itemView.findViewById(R.id.PhotoFacesName);



        }

        public void bind(PhotoFacesDataModel photoFacesDataModel) {

            if (photoFacesDataModel.getIsAnger()){
                photoFacesEmotion.setImageResource(R.drawable.angry_face);
            }
            else if (photoFacesDataModel.getIsContempt()){
                photoFacesEmotion.setImageResource(R.drawable.contempt_face);
            }
            else if (photoFacesDataModel.getIsDisgust()){
                photoFacesEmotion.setImageResource(R.drawable.disgust_face);
            }
            else if (photoFacesDataModel.getIsFear()){
                photoFacesEmotion.setImageResource(R.drawable.fear_face);
            }
            else if (photoFacesDataModel.getIsHappiness()){
                photoFacesEmotion.setImageResource(R.drawable.happiness_face);
            }
            else if (photoFacesDataModel.getIsNeutral()){
                photoFacesEmotion.setImageResource(R.drawable.neutral_face);
            }
            else if (photoFacesDataModel.getIsSadness()){
                photoFacesEmotion.setImageResource(R.drawable.sadness_face);
            }
            else if (photoFacesDataModel.getIsSurprise()){
                photoFacesEmotion.setImageResource(R.drawable.surprise_face);
            }
            GlideUrl glideUrl=new GlideUrl("http://huni.muhammedtahaikiz.com/api/Photo/GetPhotoFaceByID?photoFaceId="+photoFacesDataModel.getID(),new LazyHeaders.Builder()
                    .addHeader("Authorization", "Bearer "+ PreferencesHuni.getValue(GeneralValues.LOGIN_ACCESS_TOKEN))
                    .build());
            Glide.with(context)
                    .load(glideUrl)
                    .placeholder(R.drawable.photo_faces)
                    .crossFade()
                    .into(photoFace);


        }


    }
}
