package com.huni.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hltognc on 18.12.2016.
 */
public class PhotoFacesDataModel {
    @SerializedName("FullName")
    @Expose
    private Object fullName;
    @SerializedName("AngerConfidence")
    @Expose
    private Float angerConfidence;
    @SerializedName("ContemptConfidence")
    @Expose
    private Float contemptConfidence;
    @SerializedName("DisgustConfidence")
    @Expose
    private Float disgustConfidence;
    @SerializedName("FearConfidence")
    @Expose
    private Float fearConfidence;
    @SerializedName("HappinessConfidence")
    @Expose
    private Float happinessConfidence;
    @SerializedName("NeutralConfidence")
    @Expose
    private Float neutralConfidence;
    @SerializedName("SadnessConfidence")
    @Expose
    private Float sadnessConfidence;
    @SerializedName("SurpriseConfidence")
    @Expose
    private Float surpriseConfidence;
    @SerializedName("IsAnger")
    @Expose
    private Boolean isAnger;
    @SerializedName("IsContempt")
    @Expose
    private Boolean isContempt;
    @SerializedName("IsDisgust")
    @Expose
    private Boolean isDisgust;
    @SerializedName("IsFear")
    @Expose
    private Boolean isFear;
    @SerializedName("IsHappiness")
    @Expose
    private Boolean isHappiness;
    @SerializedName("IsNeutral")
    @Expose
    private Boolean isNeutral;
    @SerializedName("IsSadness")
    @Expose
    private Boolean isSadness;
    @SerializedName("IsSurprise")
    @Expose
    private Boolean isSurprise;
    @SerializedName("PersonConfidence")
    @Expose
    private Object personConfidence;
    @SerializedName("ID")
    @Expose
    private Integer iD;

    public Object getFullName() {
        return fullName;
    }

    public void setFullName(Object fullName) {
        this.fullName = fullName;
    }

    public Float getAngerConfidence() {
        return angerConfidence;
    }

    public void setAngerConfidence(Float angerConfidence) {
        this.angerConfidence = angerConfidence;
    }

    public Float getContemptConfidence() {
        return contemptConfidence;
    }

    public void setContemptConfidence(Float contemptConfidence) {
        this.contemptConfidence = contemptConfidence;
    }

    public Float getDisgustConfidence() {
        return disgustConfidence;
    }

    public void setDisgustConfidence(Float disgustConfidence) {
        this.disgustConfidence = disgustConfidence;
    }

    public Float getFearConfidence() {
        return fearConfidence;
    }

    public void setFearConfidence(Float fearConfidence) {
        this.fearConfidence = fearConfidence;
    }

    public Float getHappinessConfidence() {
        return happinessConfidence;
    }

    public void setHappinessConfidence(Float happinessConfidence) {
        this.happinessConfidence = happinessConfidence;
    }

    public Float getNeutralConfidence() {
        return neutralConfidence;
    }

    public void setNeutralConfidence(Float neutralConfidence) {
        this.neutralConfidence = neutralConfidence;
    }

    public Float getSadnessConfidence() {
        return sadnessConfidence;
    }

    public void setSadnessConfidence(Float sadnessConfidence) {
        this.sadnessConfidence = sadnessConfidence;
    }

    public Float getSurpriseConfidence() {
        return surpriseConfidence;
    }

    public void setSurpriseConfidence(Float surpriseConfidence) {
        this.surpriseConfidence = surpriseConfidence;
    }

    public Boolean getIsAnger() {
        return isAnger;
    }

    public void setIsAnger(Boolean isAnger) {
        this.isAnger = isAnger;
    }

    public Boolean getIsContempt() {
        return isContempt;
    }

    public void setIsContempt(Boolean isContempt) {
        this.isContempt = isContempt;
    }

    public Boolean getIsDisgust() {
        return isDisgust;
    }

    public void setIsDisgust(Boolean isDisgust) {
        this.isDisgust = isDisgust;
    }

    public Boolean getIsFear() {
        return isFear;
    }

    public void setIsFear(Boolean isFear) {
        this.isFear = isFear;
    }

    public Boolean getIsHappiness() {
        return isHappiness;
    }

    public void setIsHappiness(Boolean isHappiness) {
        this.isHappiness = isHappiness;
    }

    public Boolean getIsNeutral() {
        return isNeutral;
    }

    public void setIsNeutral(Boolean isNeutral) {
        this.isNeutral = isNeutral;
    }

    public Boolean getIsSadness() {
        return isSadness;
    }

    public void setIsSadness(Boolean isSadness) {
        this.isSadness = isSadness;
    }

    public Boolean getIsSurprise() {
        return isSurprise;
    }

    public void setIsSurprise(Boolean isSurprise) {
        this.isSurprise = isSurprise;
    }

    public Object getPersonConfidence() {
        return personConfidence;
    }

    public void setPersonConfidence(Object personConfidence) {
        this.personConfidence = personConfidence;
    }

    public Integer getID() {
        return iD;
    }

    public void setID(Integer iD) {
        this.iD = iD;
    }
}
