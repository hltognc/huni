package com.huni.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserInfoModel {

    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("FirstName")
    @Expose
    private String firstName;
    @SerializedName("LastName")
    @Expose
    private String lastName;
    @SerializedName("ProfilePhotoID")
    @Expose
    private int profilePhotoID;
    @SerializedName("CoverPhotoID")
    @Expose
    private int coverPhotoID;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getProfilePhotoID() {
        return profilePhotoID;
    }

    public void setProfilePhotoID(int profilePhotoID) {
        this.profilePhotoID = profilePhotoID;
    }

    public int getCoverPhotoID() {
        return coverPhotoID;
    }

    public void setCoverPhotoID(int coverPhotoID) {
        this.coverPhotoID = coverPhotoID;
    }
}