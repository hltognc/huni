package com.huni.model;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by hltognc on 27.11.2016.
 */

public class ImageModel implements Parcelable {


    private String bucket;
    private String date;
    private Uri imageUri;

    public ImageModel(String bucket, String date,Uri imageUri,int photoId){
        this.bucket=bucket;
        this.date=date;
        this.imageUri=imageUri;

    }


    public ImageModel() {

    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    public Uri getImageUri() {
        return imageUri;
    }

    public void setImageUri(Uri imageUri) {
        this.imageUri = imageUri;
    }







    protected ImageModel(Parcel in) {
        bucket = in.readString();
        date = in.readString();
        imageUri = Uri.parse(in.readString());

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(bucket);
        dest.writeString(date);
        dest.writeString(imageUri.toString());

    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ImageModel> CREATOR = new Parcelable.Creator<ImageModel>() {
        @Override
        public ImageModel createFromParcel(Parcel in) {
            return new ImageModel(in);
        }

        @Override
        public ImageModel[] newArray(int size) {
            return new ImageModel[size];
        }
    };


}