package com.huni.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.huni.model.DataAttModel;

import java.util.List;

/**
 * Created by hltognc on 17.12.2016.
 */

public class PhotoAttributeModel {
    @SerializedName("Result")
    @Expose
    private Integer result;
    @SerializedName("ResultMessage")
    @Expose
    private String resultMessage;
    @SerializedName("Data")
    @Expose
    private List<DataAttModel> data = null;
    @SerializedName("Errors")
    @Expose
    private List<Object> errors = null;

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

    public List<DataAttModel> getData() {
        return data;
    }

    public void setData(List<DataAttModel> data) {
        this.data = data;
    }

    public List<Object> getErrors() {
        return errors;
    }

    public void setErrors(List<Object> errors) {
        this.errors = errors;
    }
}
