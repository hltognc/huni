package com.huni.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hltognc on 17.12.2016.
 */

public class DataAttModel implements Parcelable {
    @SerializedName("AttributeName")
    @Expose
    private String attributeName;
    @SerializedName("AttributeValue")
    @Expose
    private String attributeValue;
    @SerializedName("ConfidenceScore")
    @Expose
    private Float confidenceScore;

    public DataAttModel(String attributeName, String attributeValue, Float confidenceScore) {
        this.attributeName = attributeName;
        this.attributeValue = attributeValue;
        this.confidenceScore = confidenceScore;
    }



    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    public String getAttributeValue() {
        return attributeValue;
    }

    public void setAttributeValue(String attributeValue) {
        this.attributeValue = attributeValue;
    }

    public Float getConfidenceScore() {
        return confidenceScore;
    }

    public void setConfidenceScore(Float confidenceScore) {
        this.confidenceScore = confidenceScore;
    }

    protected DataAttModel(Parcel in) {
        attributeName = in.readString();
        attributeValue = in.readString();
        confidenceScore = in.readByte() == 0x00 ? null : in.readFloat();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int i) {
        dest.writeString(attributeName);
        dest.writeString(attributeValue);
        if (confidenceScore == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeFloat(confidenceScore);
        }

    }

    public static final Creator<DataAttModel> CREATOR = new Creator<DataAttModel>() {
        @Override
        public DataAttModel createFromParcel(Parcel in) {
            return new DataAttModel(in);
        }

        @Override
        public DataAttModel[] newArray(int size) {
            return new DataAttModel[size];
        }
    };
}
