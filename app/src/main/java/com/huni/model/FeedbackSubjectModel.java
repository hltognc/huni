package com.huni.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by hltognc on 10.01.2017.
 */

public class FeedbackSubjectModel {
    @SerializedName("Result")
    @Expose
    private Integer result;
    @SerializedName("ResultMessage")
    @Expose
    private Object resultMessage;
    @SerializedName("Data")
    @Expose
    private List<FeedbackSubjectDataModel> data = null;
    @SerializedName("Errors")
    @Expose
    private List<Object> errors = null;

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

    public Object getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(Object resultMessage) {
        this.resultMessage = resultMessage;
    }

    public List<FeedbackSubjectDataModel> getData() {
        return data;
    }

    public void setData(List<FeedbackSubjectDataModel> data) {
        this.data = data;
    }

    public List<Object> getErrors() {
        return errors;
    }

    public void setErrors(List<Object> errors) {
        this.errors = errors;
    }


}
