package com.huni.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by hltognc on 12.12.2016.
 */

public class UploadPhotoModel {

    @SerializedName("Result")
    @Expose
    private Integer result;
    @SerializedName("ResultMessage")
    @Expose
    private String resultMessage;
    @SerializedName("Data")
    @Expose
    private String data;
    @SerializedName("Errors")
    @Expose
    private List<Object> errors = null;

    /**
     *
     * @return
     * The result
     */
    public Integer getResult() {
        return result;
    }

    /**
     *
     * @param result
     * The Result
     */
    public void setResult(Integer result) {
        this.result = result;
    }

    /**
     *
     * @return
     * The resultMessage
     */
    public String getResultMessage() {
        return resultMessage;
    }

    /**
     *
     * @param resultMessage
     * The ResultMessage
     */
    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

    /**
     *
     * @return
     * The data
     */
    public String getData() {
        return data;
    }

    /**
     *
     * @param data
     * The Data
     */
    public void setData(String data) {
        this.data = data;
    }

    /**
     *
     * @return
     * The errors
     */
    public List<Object> getErrors() {
        return errors;
    }

    /**
     *
     * @param errors
     * The Errors
     */
    public void setErrors(List<Object> errors) {
        this.errors = errors;
    }
}
