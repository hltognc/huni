package com.huni.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.huni.GeneralValues;
import com.huni.HuniUtil;
import com.huni.PhotoUploadService;
import com.huni.PreferencesHuni;
import com.huni.R;
import com.huni.RestInterfaceController;

import com.huni.activity.MainActivity;
import com.huni.model.UpdateUserInfoModel;
import com.huni.model.UploadPhotoModel;
import com.huni.model.UserInfoModel;
import com.ipaulpro.afilechooser.utils.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.app.Activity.RESULT_OK;

/**
 * Created by hltognc on 8.01.2017.
 */

public class ProfileFragment extends android.support.v4.app.Fragment {

    //UI
    private EditText userEmail, userFirstName, userLastName;
    private Button submitBtn;
    private CircleImageView userProfileImage;
    private ImageView userCoverImage;
    private Button pickProfilePicBtn, pickCoverPicBtn;
    private EditText oldPasswordEditText, newPasswordEditText, newPasswordConfEditText;


    private RestInterfaceController serviceAPI;
    private ProgressDialog progressDialog;
    private ProgressDialog progressDialog2;
    View focusView = null;


    private static final int PICK_PROFILE_IMAGE_FROM_GALLERY_ACTIVITY_REQUEST_CODE = 0;
    private static final int PICK_COVER_IMAGE_FROM_GALLERY_ACTIVITY_REQUEST_CODE = 1;

    //for update info
    String userEmailStr, userFirstNameStr, userLastNameStr;
    int userProfilePhotoId, userCoverPhotoId;
    String userOldPasswordStr, userNewPasswordStr, userNewPasswordConfirmationStr;

    private String TAG = getClass().getName();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        userEmail = (EditText) view.findViewById(R.id.UserEmail);
        userFirstName = (EditText) view.findViewById(R.id.UserFirstName);
        userLastName = (EditText) view.findViewById(R.id.UserLastName);
        submitBtn = (Button) view.findViewById(R.id.submitBtn);
        userProfileImage = (CircleImageView) view.findViewById(R.id.profile_image);
        userCoverImage = (ImageView) view.findViewById(R.id.cover_image);
        pickProfilePicBtn = (Button) view.findViewById(R.id.PickProfilePic);
        pickCoverPicBtn = (Button) view.findViewById(R.id.PickCoverPicture);
        oldPasswordEditText = (EditText) view.findViewById(R.id.UserOldPassword);
        newPasswordEditText = (EditText) view.findViewById(R.id.UserNewPassword);
        newPasswordConfEditText = (EditText) view.findViewById(R.id.UserPasswordConfirmation);

        setCurrentValues();
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UpdateUserInfo(userEmail.getText().toString(), userFirstName.getText().toString(),
                        userLastName.getText().toString(), userProfilePhotoId, userCoverPhotoId);
                if (!TextUtils.isEmpty(oldPasswordEditText.getText().toString()) ||
                        !TextUtils.isEmpty(newPasswordEditText.getText().toString()) ||
                        !TextUtils.isEmpty(newPasswordConfEditText.getText().toString())) {
                    attemptChangePassword();
                }

            }
        });

        pickProfilePicBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, PICK_PROFILE_IMAGE_FROM_GALLERY_ACTIVITY_REQUEST_CODE);
            }
        });

        pickCoverPicBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, PICK_COVER_IMAGE_FROM_GALLERY_ACTIVITY_REQUEST_CODE);
            }
        });

        return view;
    }

    private void attemptChangePassword() {
        boolean mCancel = this.changePasswordValidation();
        if (mCancel) {
            focusView.requestFocus();
        } else {
            changePassword(userOldPasswordStr, userNewPasswordStr, userNewPasswordConfirmationStr);
        }

    }

    private void changePassword(String userOldPasswordStr, String userNewPasswordStr, String userNewPasswordConfirmationStr) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(GeneralValues.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        serviceAPI = retrofit.create(RestInterfaceController.class);
        Call<Void> call = serviceAPI.changePassword("Bearer " + PreferencesHuni.getValue(GeneralValues.LOGIN_ACCESS_TOKEN),
                userOldPasswordStr, userNewPasswordStr, userNewPasswordConfirmationStr);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.code() == 200) {
                    HuniUtil.createAlertDialog(getActivity(), "Password has been changed ", false);
                } else {
                    HuniUtil.createAlertDialog(getActivity(), "Your old password is wrong.", false);
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                HuniUtil.createSnackbar(getActivity(), getResources().getString(R.string.internet_connection_problem));
            }
        });

    }


    private boolean changePasswordValidation() {
        boolean cancel = false;
        oldPasswordEditText.setError(null);
        newPasswordEditText.setError(null);
        newPasswordConfEditText.setError(null);

        userOldPasswordStr = oldPasswordEditText.getText().toString();
        userNewPasswordStr = newPasswordEditText.getText().toString();
        userNewPasswordConfirmationStr = newPasswordConfEditText.getText().toString();

        if (userNewPasswordConfirmationStr.length() < 6) {
            newPasswordConfEditText.setError(getString(R.string.password_length_failed));
            focusView = newPasswordConfEditText;
            cancel = true;
        }

        if (userNewPasswordStr.length() < 6) {
            newPasswordEditText.setError(getString(R.string.password_length_failed));
            focusView = newPasswordEditText;
            cancel = true;
        }
        if (userOldPasswordStr.length() < 6) {
            oldPasswordEditText.setError(getString(R.string.password_length_failed));
            focusView = oldPasswordEditText;
            cancel = true;
        }
        if (TextUtils.isEmpty(userNewPasswordConfirmationStr)) {
            newPasswordConfEditText.setError(getString(R.string.error_field_required));
            focusView = newPasswordConfEditText;
            cancel = true;
        }
        if (TextUtils.isEmpty(userNewPasswordStr)) {
            newPasswordEditText.setError(getString(R.string.error_field_required));
            focusView = newPasswordEditText;
            cancel = true;
        }
        if (TextUtils.isEmpty(userOldPasswordStr)) {
            oldPasswordEditText.setError(getString(R.string.error_field_required));
            focusView = oldPasswordEditText;
            cancel = true;
        }

        if (!userNewPasswordStr.equals(userNewPasswordConfirmationStr)) {
            newPasswordConfEditText.setError(getString(R.string.password_confirmation_failed));
            focusView = newPasswordConfEditText;
            cancel = true;
        }
        return cancel;
    }

    private void UpdateUserInfo(String userEmailStr, String userFirstNameStr, String userLastNameStr, int userProfilePhotoId, int userCoverPhotoId) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(GeneralValues.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        serviceAPI = retrofit.create(RestInterfaceController.class);


        retrofit2.Call<UpdateUserInfoModel> call = serviceAPI.updateUserInfo("Bearer " + PreferencesHuni.getValue(GeneralValues.LOGIN_ACCESS_TOKEN),
                userEmailStr, userFirstNameStr, userLastNameStr, userProfilePhotoId, userCoverPhotoId);

        call.enqueue(new Callback<UpdateUserInfoModel>() {
            @Override
            public void onResponse(Call<UpdateUserInfoModel> call, Response<UpdateUserInfoModel> response) {

                if (response.isSuccessful()) {
                    setCurrentValues();
                    ((MainActivity)getActivity()).setHeaderCurrentValues();
                } else {
                    HuniUtil.createSnackbar(getActivity(), getResources().getString(R.string.internet_connection_problem));
                }

            }

            @Override
            public void onFailure(Call<UpdateUserInfoModel> call, Throwable t) {

                HuniUtil.createSnackbar(getActivity(), getResources().getString(R.string.internet_connection_problem));
            }
        });


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            Uri selectedImageUri = data.getData();
            if (requestCode == PICK_PROFILE_IMAGE_FROM_GALLERY_ACTIVITY_REQUEST_CODE) {
                Glide.with(getActivity())
                        .load(selectedImageUri)
                        .placeholder(R.drawable.profile_pic_place_holder)
                        .dontAnimate()
                        .into(userProfileImage);
                uploadPhoto(true, selectedImageUri);

            } else if (requestCode == PICK_COVER_IMAGE_FROM_GALLERY_ACTIVITY_REQUEST_CODE) {
                Glide.with(getActivity())
                        .load(selectedImageUri)
                        .placeholder(R.drawable.cover_photo_place_holder)
                        .dontAnimate()
                        .into(userCoverImage);
                uploadPhoto(false, selectedImageUri);
            }
        }

    }

    private void uploadPhoto(final boolean isThataProfilPhoto, Uri selectedImageUri) {

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(5, TimeUnit.MINUTES)
                .readTimeout(5, TimeUnit.MINUTES)
                .writeTimeout(5, TimeUnit.MINUTES)
                .build();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(GeneralValues.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        serviceAPI = retrofit.create(RestInterfaceController.class);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();

        File file = FileUtils.getFile(getActivity(), selectedImageUri);
        RequestBody fbody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part imageFileBody = MultipartBody.Part.createFormData("image", file.getName(), fbody);
        Call<UploadPhotoModel> call = serviceAPI.uploadPhoto("Bearer " + PreferencesHuni.getValue(GeneralValues.LOGIN_ACCESS_TOKEN),
                imageFileBody);
        call.enqueue(new Callback<UploadPhotoModel>() {
            @Override
            public void onResponse(Call<UploadPhotoModel> call, Response<UploadPhotoModel> response) {
                if (response.isSuccessful()) {
                    int id = Integer.parseInt(response.body().getData().split(":")[1]);
                    if (isThataProfilPhoto) {
                        userProfilePhotoId = id;
                    } else {
                        userCoverPhotoId = id;
                    }
                    Log.e(TAG, "Id= " + id);
                } else {
                    HuniUtil.createSnackbar(getActivity(), getResources().getString(R.string.internet_connection_problem));
                }
                progressDialog.cancel();
            }

            @Override
            public void onFailure(Call<UploadPhotoModel> call, Throwable t) {
                HuniUtil.createSnackbar(getActivity(), getResources().getString(R.string.internet_connection_problem));
                progressDialog.cancel();
            }
        });
    }

    private void setCurrentValues() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(GeneralValues.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        serviceAPI = retrofit.create(RestInterfaceController.class);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();
        retrofit2.Call<UserInfoModel> call = serviceAPI.getUserInfo("Bearer " + PreferencesHuni.getValue(GeneralValues.LOGIN_ACCESS_TOKEN));
        call.enqueue(new Callback<UserInfoModel>() {
            @Override
            public void onResponse(Call<UserInfoModel> call, Response<UserInfoModel> response) {

                if (response.code() == 200) {
                    userEmailStr = response.body().getEmail();
                    userFirstNameStr = response.body().getFirstName();
                    userLastNameStr = response.body().getLastName();
                    userProfilePhotoId = response.body().getProfilePhotoID();
                    userCoverPhotoId = response.body().getCoverPhotoID();

                    userEmail.setText(userEmailStr);
                    userFirstName.setText(userFirstNameStr);
                    userLastName.setText(userLastNameStr);
                    GlideUrl glideUrl = new GlideUrl("http://huni.muhammedtahaikiz.com/api/Photo/GetPhotoByID?photoId=" + userProfilePhotoId
                            , new LazyHeaders.Builder()
                            .addHeader("Authorization", "Bearer " + PreferencesHuni.getValue(GeneralValues.LOGIN_ACCESS_TOKEN))
                            .build());
                    Glide.with(getActivity())
                            .load(glideUrl)
                            .placeholder(R.drawable.profile_pic_place_holder)
                            .dontAnimate()
                            .into(userProfileImage);

                    GlideUrl glideUrl2 = new GlideUrl("http://huni.muhammedtahaikiz.com/api/Photo/GetPhotoByID?photoId=" + userCoverPhotoId
                            , new LazyHeaders.Builder()
                            .addHeader("Authorization", "Bearer " + PreferencesHuni.getValue(GeneralValues.LOGIN_ACCESS_TOKEN))
                            .build());
                    Glide.with(getActivity())
                            .load(glideUrl2)
                            .placeholder(R.drawable.cover_photo_place_holder)
                            .dontAnimate()
                            .into(userCoverImage);

                }
                progressDialog.cancel();
            }

            @Override
            public void onFailure(Call<UserInfoModel> call, Throwable t) {
                progressDialog.cancel();
            }
        });


    }


    public Bitmap reSizeImage(Uri uri){
        Bitmap bitmap=null;
        try {

            InputStream imageStream = getActivity().getContentResolver().openInputStream(uri);
            bitmap = BitmapFactory.decodeStream(imageStream);

            int imageWidth = bitmap.getWidth();
            int imageHeight = bitmap.getHeight();
            double ratio;

            if (imageHeight > 1000 || imageWidth > 1000){
                if (imageHeight > imageWidth){
                    ratio = roundMyResult((double)1000/imageHeight,5);
                }else{
                    ratio = roundMyResult((double)1000/imageWidth,5);
                }
            }else{
                ratio = 1;
            }

            Matrix matrix = new Matrix();
            matrix.postScale((float)ratio,(float) ratio);
            bitmap = Bitmap.createBitmap(bitmap,0,0,imageWidth,imageHeight,matrix,true);


        }catch (FileNotFoundException e){
            e.printStackTrace();

        }
        return bitmap;
    }

    public static double roundMyResult(double val, int numberOfDigitsAfterDecimal) {
        double p = (float)Math.pow(10,numberOfDigitsAfterDecimal);
        val = val * p;
        double tmp = Math.floor(val);
        return tmp/p;
    }

}
