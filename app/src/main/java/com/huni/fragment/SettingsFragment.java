package com.huni.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.huni.GeneralValues;
import com.huni.PreferencesHuni;
import com.huni.R;

/**
 * Created by hltognc on 7.01.2017.
 */

public class SettingsFragment extends Fragment {

    private TextView aboutHuniText;

    private ToggleButton wifiToggleBtn;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        aboutHuniText = (TextView) view.findViewById(R.id.aboutHuni);
        wifiToggleBtn = (ToggleButton) view.findViewById(R.id.wifiToggle);
        try {
            if (PreferencesHuni.checkPreferencesWhetherTheValueisExistorNot(GeneralValues.SETTINGS_USE_ONLY_WIFI)){
                if (PreferencesHuni.getValue(GeneralValues.SETTINGS_USE_ONLY_WIFI).equals("1")) {
                    wifiToggleBtn.setChecked(true);
                } else if (PreferencesHuni.getValue(GeneralValues.SETTINGS_USE_ONLY_WIFI).equals("0")) {
                    wifiToggleBtn.setChecked(false);
                }
            }

        } catch (Exception e) {
            wifiToggleBtn.setChecked(false);
        }
        aboutHuniText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AboutHuniFragment aboutHuniFragment = new AboutHuniFragment();
                FragmentManager manager = getActivity().getSupportFragmentManager();
                manager.beginTransaction().replace(R.id.content_main,
                        aboutHuniFragment).addToBackStack("aboutHuni").commit();


            }
        });

        wifiToggleBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    PreferencesHuni.setValue(GeneralValues.SETTINGS_USE_ONLY_WIFI, "1");
                } else {
                    PreferencesHuni.setValue(GeneralValues.SETTINGS_USE_ONLY_WIFI, "0");
                }
            }
        });
        return view;
    }
}
