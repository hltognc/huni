package com.huni.fragment;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.huni.DatabaseHelper;
import com.huni.GeneralValues;
import com.huni.HuniUtil;
import com.huni.PreferencesHuni;
import com.huni.RestInterfaceController;
import com.huni.adapter.GalleryAdapter;
import com.huni.PhotoUploadService;
import com.huni.R;
import com.huni.model.ImageFilterModel;
import com.huni.model.ImageModel;


import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;

/**
 * Created by hltognc on 27.11.2016.
 */

public class GalleryFragment extends Fragment implements SearchView.OnQueryTextListener {

    private static final int REQUEST_READ_EXTERNAL_STORAGE = 0;


    private List<ImageModel> imagesArrayList = new ArrayList<ImageModel>();
    private ProgressDialog pDialog;
    private GalleryAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private TextView noPhotosText;
    private RestInterfaceController serviceAPI;
    private PhotoUploadService mPhotoUploadService;
    boolean mBound = false;
    private List<Integer> filteredPhotoIDs;
    private List<Integer> userImagesPhotoIDs;
    private Cursor cursor;
    private Uri images;
    String[] projection;
    private DatabaseHelper huniDb;
    private ProgressDialog progressDialog;
    private SearchView searchView;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gallery, container, false);
        setHasOptionsMenu(true);
        huniDb = new DatabaseHelper(getActivity());
        huniDb.getReadableDatabase();

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        noPhotosText = (TextView) view.findViewById(R.id.noPhotosText);

        int permissionCheck = ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.READ_EXTERNAL_STORAGE);
        pDialog = new ProgressDialog(getActivity());
        imagesArrayList = new ArrayList<>();
        projection = new String[]{
                MediaStore.Images.Media._ID,
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME,
                MediaStore.Images.Media.DATE_TAKEN
        };
        images = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;


        fetchImages(projection, images);

        Intent serviceIntent = new Intent(getActivity(), PhotoUploadService.class);
//        Bundle serviceBundle=new Bundle();
//        serviceBundle.putSerializable("images",imagesArrayList);
        serviceIntent.putParcelableArrayListExtra("images", (ArrayList<? extends Parcelable>) imagesArrayList);
//        serviceIntent.putExtras(serviceBundle);
        getActivity().startService(serviceIntent);

        mRecyclerView.addOnItemTouchListener(new GalleryAdapter.RecyclerTouchListener(getActivity(),
                mRecyclerView, new GalleryAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("images", (Serializable) imagesArrayList);
                bundle.putInt("position", position);


                SlideShowDialogFragment slideShowDialogFragment = SlideShowDialogFragment.newInstance();
                slideShowDialogFragment.setArguments(bundle);
                FragmentManager manager = getActivity().getSupportFragmentManager();
                manager.beginTransaction().replace(R.id.content_main,
                        slideShowDialogFragment,
                        slideShowDialogFragment.getTag())
                        .addToBackStack(null)
                        .commit();


//                slideShowDialogFragment.show(ft, "slideshow");
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
        final MenuItem item = menu.findItem(R.id.action_search);
        searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);
        searchView.clearFocus();

    }

    @Override
    public boolean onQueryTextSubmit(String searchText) {
        filter(imagesArrayList, searchText);
        searchView.clearFocus();
        return true;
    }

    @Override
    public boolean onQueryTextChange(String searchText) {
        if (searchText.length() == 0) {
            mAdapter.animateTo(imagesArrayList);
            mRecyclerView.scrollToPosition(0);
            noPhotosText.setVisibility(View.GONE);
            searchView.clearFocus();
        }
        return true;
    }

    private void filter(final List<ImageModel> imagesArrayList, String searchText) {
        searchText = searchText.toLowerCase();
        final List<ImageModel> filteredImageList = new ArrayList<>();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(GeneralValues.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        serviceAPI = retrofit.create(RestInterfaceController.class);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();
        retrofit2.Call<ImageFilterModel> call = serviceAPI.getPhotoByFilter("Bearer " + PreferencesHuni.getValue(GeneralValues.LOGIN_ACCESS_TOKEN),
                searchText);


        final String finalSearchText = searchText;
        call.enqueue(new Callback<ImageFilterModel>() {
            @Override
            public void onResponse(Call<ImageFilterModel> call, Response<ImageFilterModel> response) {
                try {
                    if (response.body().getResult() == 1) {
                        filteredPhotoIDs = new ArrayList<>();
                        userImagesPhotoIDs = new ArrayList<Integer>();
                        userImagesPhotoIDs = huniDb.getUserImagesPhotoId();
                        filteredPhotoIDs = response.body().getData();

                        for (int i = 0; i < filteredPhotoIDs.size(); i++) {
                            for (int j = 0; j < userImagesPhotoIDs.size(); j++) {
                                if (filteredPhotoIDs.get(i).equals(userImagesPhotoIDs.get(j))) {
                                    ImageModel imageModel = new ImageModel();
                                    imageModel.setImageUri(Uri.parse(huniDb.getPhotoUriById(userImagesPhotoIDs.get(j))));
                                    filteredImageList.add(imageModel);
                                }
                            }
                        }

                        if (finalSearchText.length() < 1) {
                            mAdapter.animateTo(imagesArrayList);
                            mRecyclerView.scrollToPosition(0);
                            noPhotosText.setVisibility(View.GONE);
                        } else {
                            mAdapter.animateTo(filteredImageList);
                            mRecyclerView.scrollToPosition(0);
                            noPhotosText.setVisibility(View.GONE);
                        }

//                        for (int i = 0; i < filteredPhotoIDs.size(); i++) {
//                            if (!huniDb.getPhotoUriById(filteredPhotoIDs.get(i)).equals("")) {
//                                for (int j = 0; i < imagesArrayList.size(); j++) {
//                                    if (imagesArrayList.get(j).getImageUri().equals(huniDb.getPhotoUriById(filteredPhotoIDs.get(i)))) {
//                                        filteredImageList.add(imagesArrayList.get(j));
//                                        j=imagesArrayList.size();
//                                    }
//
//                                }
//                            }
//                        }
                        progressDialog.cancel();


                    } else if (response.body().getResult() == 0) {
                        //hiçbirşey bulamadım demek
                        filteredImageList.clear();
                        mAdapter.animateTo(filteredImageList);
                        mRecyclerView.scrollToPosition(0);
                        progressDialog.cancel();

                        noPhotosText.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    HuniUtil.createSnackbar(getActivity(), getResources().getString(R.string.internet_connection_problem));
                    filteredImageList.clear();
                    mAdapter.animateTo(filteredImageList);
                    mRecyclerView.scrollToPosition(0);
                    progressDialog.cancel();
                    noPhotosText.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onFailure(Call<ImageFilterModel> call, Throwable t) {
                HuniUtil.createSnackbar(getActivity(), getResources().getString(R.string.internet_connection_problem));
                filteredImageList.clear();
                mAdapter.animateTo(filteredImageList);
                mRecyclerView.scrollToPosition(0);
                progressDialog.cancel();
                noPhotosText.setVisibility(View.VISIBLE);
            }
        });

    }


    private void fetchImages(final String[] projection, Uri images) {
        if (!mayRequestExternalStorage()) {
            return;
        }

        // Uri newUri=Uri.parse(images.toString()+"/Picures/");
        cursor = getActivity().getContentResolver().query(images,
                projection, // Which columns to return
                null,       // Which rows to return (all rows)
                null,       // Selection arguments (none)
                null        // Ordering
        );


        pDialog.setMessage(getString(R.string.loading));
        pDialog.show();

        int bucketColumn = cursor.getColumnIndex(
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME);

        int dateColumn = cursor.getColumnIndex(
                MediaStore.Images.Media.DATE_TAKEN);

        imagesArrayList.clear();
        for (; cursor.moveToNext(); ) {
            ImageModel image = new ImageModel();
            image.setBucket(cursor.getString(bucketColumn));
            image.setDate(cursor.getString(dateColumn));
            image.setImageUri(ContentUris
                    .withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                            cursor.getInt(cursor.getColumnIndex(MediaStore.Images.ImageColumns._ID))));

            imagesArrayList.add(image);
        }
        // mAdapter.notifyDataSetChanged();
        pDialog.hide();

        mAdapter = new GalleryAdapter(getActivity(), imagesArrayList);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter.notifyDataSetChanged();
        mRecyclerView.setAdapter(mAdapter);


    }


    private boolean mayRequestExternalStorage() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (getActivity().checkSelfPermission(READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_EXTERNAL_STORAGE)) {
            Snackbar.make(mRecyclerView, "", Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @RequiresApi(api = Build.VERSION_CODES.M)
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{READ_EXTERNAL_STORAGE}, REQUEST_READ_EXTERNAL_STORAGE);
                        }
                    });
        } else {
            requestPermissions(new String[]{READ_EXTERNAL_STORAGE}, REQUEST_READ_EXTERNAL_STORAGE);
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_EXTERNAL_STORAGE) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                fetchImages(projection, images);
            }
        }
    }


}
