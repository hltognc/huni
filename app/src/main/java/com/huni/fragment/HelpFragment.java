package com.huni.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.huni.R;
import com.huni.adapter.ExpandableListCustomAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by hltognc on 10.01.2017.
 */

public class HelpFragment extends Fragment {

    private ExpandableListCustomAdapter expandableListAdapter;
    private ExpandableListView expandableListView;
    private List<String> listDataHeader;
    private HashMap<String,List<String>> listDataChild;

    private View rootView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView=inflater.inflate(R.layout.fragment_help,container,false);
        expandableListView= (ExpandableListView) rootView.findViewById(R.id.expandableListView);
        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            int lastClickedPosition=-1;
            @Override
            public boolean onGroupClick(ExpandableListView parent, View view, int groupPosition, long l) {
                Boolean shouldExpand=(!expandableListView.isGroupExpanded(groupPosition));
                if (groupPosition!=lastClickedPosition){
                    expandableListView.collapseGroup(lastClickedPosition);
                }
                if (shouldExpand){
                    expandableListView.expandGroup(groupPosition);
                    expandableListView.setSelectionFromTop(groupPosition,0);
                }
                lastClickedPosition=groupPosition;
                return true;

            }
        });

        setHelpListData();

        return rootView;
    }

    private void setHelpListData() {
        listDataHeader=new ArrayList<String>();
        listDataChild=new HashMap<String,List<String>>();

        try {
            JSONObject obj = new JSONObject(loadJSONFromAsset());
            JSONArray mJArry = obj.getJSONArray("help");


            for (int i = 0; i < mJArry.length(); i++) {
                JSONObject jo_inside = mJArry.getJSONObject(i);

                String title = jo_inside.getString("title");
                String body = jo_inside.getString("body");
                List<String> bodyListStr = new ArrayList<String>();
                bodyListStr.add(body);
                listDataHeader.add(title);
                listDataChild.put(listDataHeader.get(i),bodyListStr);


            }
            expandableListAdapter=new ExpandableListCustomAdapter(getActivity(),listDataHeader,listDataChild);
            expandableListView.setAdapter(expandableListAdapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getActivity().getAssets().open("help.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
