package com.huni.fragment;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.huni.DatabaseHelper;
import com.huni.GeneralValues;
import com.huni.HuniUtil;
import com.huni.model.PhotoAttributeModel;
import com.huni.PreferencesHuni;
import com.huni.R;
import com.huni.RestInterfaceController;
import com.huni.activity.PhotoAttributeDetailActivity;
import com.huni.activity.PhotoFacesActivity;
import com.huni.adapter.MyViewPagerAdapter;
import com.huni.adapter.PhotoAttributeAdapter;
import com.huni.model.DataAttModel;
import com.huni.model.ImageModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by hltognc on 30.11.2016.
 */

public class SlideShowDialogFragment extends android.support.v4.app.DialogFragment {

    private String TAG = SlideShowDialogFragment.class.getSimpleName();

    private ArrayList<ImageModel> images=new ArrayList<>();
    private ViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;
    private TextView countTxt;
    private TextView captionText;
    private int selectedPosition = 0;
    private RestInterfaceController serviceAPI;
    private DatabaseHelper huniDB;
    private List<DataAttModel> photoAttributeDataList = new ArrayList<>();
    private ImageButton informationBtn;
    private ImageButton photoFacesBtn;

    private String header = "";


    private PhotoAttributeAdapter photoAttributeAdapter;


    static SlideShowDialogFragment newInstance() {
        SlideShowDialogFragment slideShowDialogFragment = new SlideShowDialogFragment();
        return slideShowDialogFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_image_slider, container, false);
        countTxt = (TextView) v.findViewById(R.id.countText);
        captionText = (TextView) v.findViewById(R.id.captionText);
        viewPager = (ViewPager) v.findViewById(R.id.viewPager);
        informationBtn= (ImageButton) v.findViewById(R.id.informationBtn);
        photoFacesBtn= (ImageButton) v.findViewById(R.id.photoFacesBtn);
//        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
//                RelativeLayout.LayoutParams.MATCH_PARENT,
//                RelativeLayout.LayoutParams.WRAP_CONTENT);
//        layoutParams.height=getScreenHeight();
//        viewPager.setLayoutParams(layoutParams);

        images = (ArrayList<ImageModel>) getArguments().getSerializable("images");

        selectedPosition = getArguments().getInt("position");
        //db
        huniDB = new DatabaseHelper(getActivity());
        huniDB.getWritableDatabase();
        try {
            header = PreferencesHuni.getValue(GeneralValues.LOGIN_ACCESS_TOKEN);
        } catch (Exception e) {
            Log.e(TAG, "header catch()");
        }
        Log.e(TAG, " selectedPosition=" + selectedPosition);

        myViewPagerAdapter = new MyViewPagerAdapter(getActivity(), images);
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);

        setCurrentItem(selectedPosition);
        informationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getActivity(),PhotoAttributeDetailActivity.class);
                intent.putParcelableArrayListExtra("images_attribute", (ArrayList<? extends Parcelable>) photoAttributeDataList);
                startActivity(intent);
            }
        });

        photoFacesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getActivity(),PhotoFacesActivity.class);
                intent.putExtra("photo_id",huniDB.getPhotoIdByImageUri(images.get(selectedPosition).getImageUri().toString()));
                startActivity(intent);
            }
        });


        return v;
    }

    private void setCurrentItem(int position) {
        viewPager.setCurrentItem(position, true);
        selectedPosition=position;
        displayMetaInfo(position);
    }

    private void displayMetaInfo(int position) {
        selectedPosition=position;
        countTxt.setText((position + 1) + "of " + images.size());
        Log.e(TAG,"displayMetaInfo"+position);
        try{
            getCaptions(position);

        }
        catch (Exception e){
            HuniUtil.createSnackbar(getActivity(),"Still processing..");
            captionText.setText("");
            Log.e(TAG,"getCaptions() catch();");
        }

//        ImageModel imageModel=images.get(position);
//        titleTxt.setText(imageModel.getBucket());
//        dateTxt.setText(imageModel.getDate());


    }

    private List<DataAttModel> getCaptions(int position) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(GeneralValues.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        serviceAPI = retrofit.create(RestInterfaceController.class);

        Call<PhotoAttributeModel> call = serviceAPI.getPhotoAttributeByPhotoId("Bearer " + header,
                huniDB.getPhotoIdByImageUri(images.get(position).getImageUri().toString()));
        int id=huniDB.getPhotoIdByImageUri(images.get(position).getImageUri().toString());
        if (id==0){
            photoAttributeDataList.clear();
            return photoAttributeDataList;
        }

            call.enqueue(new Callback<PhotoAttributeModel>() {
                @Override
                public void onResponse(Call<PhotoAttributeModel> call, Response<PhotoAttributeModel> response) {
                    if (response.isSuccessful()) {
                        photoAttributeDataList = response.body().getData();
                        for (DataAttModel dataAttModel : response.body().getData()) {
                            if (dataAttModel.getAttributeName().equals("Caption")) {
                                captionText.setText(dataAttModel.getAttributeValue());

                            }
                        }


                    } else {
                        HuniUtil.createSnackbar(getActivity(), getString(R.string.internet_connection_problem));
                    }
                }

                @Override
                public void onFailure(Call<PhotoAttributeModel> call, Throwable t) {
                    Log.e(TAG, "onFailure()");
                }
            });
        return photoAttributeDataList;
    }

    //page change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            displayMetaInfo(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(android.support.v4.app.DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
    }


    private int getScreenHeight() {
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        return height;
    }


}
