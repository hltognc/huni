package com.huni.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.bumptech.glide.Glide;
import com.huni.GeneralValues;
import com.huni.HuniUtil;
import com.huni.PreferencesHuni;
import com.huni.R;
import com.huni.RestInterfaceController;
import com.huni.model.FeedbackSubjectDataModel;
import com.huni.model.FeedbackSubjectModel;
import com.huni.model.SendFeedbackModel;
import com.huni.model.UploadPhotoModel;
import com.ipaulpro.afilechooser.utils.FileUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.app.Activity.RESULT_OK;

/**
 * Created by hltognc on 10.01.2017.
 */

public class SendFeedbackFragment extends Fragment {

    private Spinner subjectSpinner;
    private EditText userMessage;
    private ImageView screenshotImage;
    private Button pickImage, submitBtn;


    private RestInterfaceController serviceAPI;
    private ProgressDialog progressDialog;
    private ProgressDialog progressDialog2;
    private List<FeedbackSubjectDataModel> feedbackSubjectDataList = new ArrayList<>();
    private List<String> spinnerItemArrayList = new ArrayList<>();
    View focusView = null;
    private static final int PICK__IMAGE_FROM_GALLERY_ACTIVITY_REQUEST_CODE = 0;

    private String userMessageStr;
    private int userPhotoId = 0;

    private String TAG = getClass().getName();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_send_feedback, container, false);
        subjectSpinner = (Spinner) view.findViewById(R.id.subject_spinner);
        userMessage = (EditText) view.findViewById(R.id.UserMessage);
        screenshotImage = (ImageView) view.findViewById(R.id.screenshot_image);
        pickImage = (Button) view.findViewById(R.id.PickPic);
        submitBtn = (Button) view.findViewById(R.id.submitBtn);
        setDefaultValues();


        pickImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, PICK__IMAGE_FROM_GALLERY_ACTIVITY_REQUEST_CODE);
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptSendFeedBack(feedbackSubjectDataList.get(subjectSpinner.getSelectedItemPosition()).getId(),
                        userMessage.getText().toString(), userPhotoId);
            }
        });
        return view;
    }

    private void attemptSendFeedBack(int subjectId, String message, int photoID) {
        boolean mCancel = this.sendFeedBackValidation();
        if (mCancel) {
            focusView.requestFocus();
        } else {
            sendFeedBack(subjectId, message, photoID);
        }
    }

    private void sendFeedBack(int subjectId, String message, int photoID) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(GeneralValues.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        serviceAPI = retrofit.create(RestInterfaceController.class);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();
        retrofit2.Call<SendFeedbackModel> call;
        if (photoID == 0) {
            call = serviceAPI.sendFeedback("Bearer " + PreferencesHuni.getValue(GeneralValues.LOGIN_ACCESS_TOKEN),
                    subjectId, message);
        } else {
            call = serviceAPI.sendFeedback("Bearer " + PreferencesHuni.getValue(GeneralValues.LOGIN_ACCESS_TOKEN),
                    subjectId, message, photoID);
        }

        call.enqueue(new Callback<SendFeedbackModel>() {
            @Override
            public void onResponse(Call<SendFeedbackModel> call, Response<SendFeedbackModel> response) {
                if (response.code()==200){
                    HuniUtil.createAlertDialog(getActivity(),"Your feedback has been sent.",false);
                }
                else {
                    HuniUtil.createSnackbar(getActivity(),getResources().getString(R.string.internet_connection_problem));
                }
                progressDialog.cancel();
            }

            @Override
            public void onFailure(Call<SendFeedbackModel> call, Throwable t) {
                progressDialog.cancel();
                HuniUtil.createSnackbar(getActivity(),getResources().getString(R.string.internet_connection_problem));

            }
        });

    }

    private boolean sendFeedBackValidation() {
        boolean cancel = false;
        userMessage.setError(null);

        userMessageStr = userMessage.getText().toString();

        if (TextUtils.isEmpty(userMessageStr)) {
            userMessage.setError(getString(R.string.error_field_required));
            focusView = userMessage;
            cancel = true;
        }

        return cancel;
    }


    private void setDefaultValues() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(GeneralValues.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        serviceAPI = retrofit.create(RestInterfaceController.class);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();
        retrofit2.Call<FeedbackSubjectModel> call = serviceAPI.getFeedbackSubjects("Bearer " + PreferencesHuni.getValue(GeneralValues.LOGIN_ACCESS_TOKEN));
        call.enqueue(new Callback<FeedbackSubjectModel>() {
            @Override
            public void onResponse(Call<FeedbackSubjectModel> call, Response<FeedbackSubjectModel> response) {

                if (response.code() == 200) {
                    feedbackSubjectDataList = response.body().getData();
                    spinnerItemArrayList = new ArrayList<String>();
                    for (FeedbackSubjectDataModel dataModel : feedbackSubjectDataList) {
                        spinnerItemArrayList.add(dataModel.getName());
                    }
                    ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item,
                            spinnerItemArrayList);
                    spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    subjectSpinner.setAdapter(spinnerArrayAdapter);
                } else {
                    HuniUtil.createSnackbar(getActivity(), getResources().getString(R.string.internet_connection_problem));
                }
                progressDialog.cancel();
            }

            @Override
            public void onFailure(Call<FeedbackSubjectModel> call, Throwable t) {
                progressDialog.cancel();
                HuniUtil.createSnackbar(getActivity(), getResources().getString(R.string.internet_connection_problem));
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            Uri selectedImageUri = data.getData();
            if (requestCode == PICK__IMAGE_FROM_GALLERY_ACTIVITY_REQUEST_CODE) {
                Glide.with(getActivity())
                        .load(selectedImageUri)
                        .placeholder(R.drawable.screenshot_place_holder)
                        .dontAnimate()
                        .into(screenshotImage);
                uploadPhoto(selectedImageUri);

            }
        }

    }

    private void uploadPhoto(Uri selectedImageUri) {

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(5, TimeUnit.MINUTES)
                .readTimeout(5, TimeUnit.MINUTES)
                .writeTimeout(5, TimeUnit.MINUTES)
                .build();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(GeneralValues.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        serviceAPI = retrofit.create(RestInterfaceController.class);
        progressDialog2 = new ProgressDialog(getActivity());
        progressDialog2.setMessage(getString(R.string.loading));
        progressDialog2.setCancelable(false);
        progressDialog2.show();

        File file = FileUtils.getFile(getActivity(), selectedImageUri);
        RequestBody fbody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part imageFileBody = MultipartBody.Part.createFormData("image", file.getName(), fbody);
        Call<UploadPhotoModel> call = serviceAPI.uploadPhoto("Bearer " + PreferencesHuni.getValue(GeneralValues.LOGIN_ACCESS_TOKEN),
                imageFileBody);
        call.enqueue(new Callback<UploadPhotoModel>() {
            @Override
            public void onResponse(Call<UploadPhotoModel> call, Response<UploadPhotoModel> response) {
                if (response.isSuccessful()) {
                    int id = Integer.parseInt(response.body().getData().split(":")[1]);

                    userPhotoId = id;

                    Log.e(TAG, "Id= " + id);
                } else {
                    HuniUtil.createSnackbar(getActivity(), getResources().getString(R.string.internet_connection_problem));
                }
                progressDialog2.cancel();
            }

            @Override
            public void onFailure(Call<UploadPhotoModel> call, Throwable t) {
                HuniUtil.createSnackbar(getActivity(), getResources().getString(R.string.internet_connection_problem));
                progressDialog2.cancel();
            }
        });
    }
}
