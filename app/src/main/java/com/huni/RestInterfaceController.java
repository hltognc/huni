package com.huni;

import com.huni.model.FeedbackSubjectModel;
import com.huni.model.ImageFilterModel;
import com.huni.model.PhotoAttributeModel;
import com.huni.model.PhotoFacesModel;
import com.huni.model.SendFeedbackModel;
import com.huni.model.UpdateUserInfoModel;
import com.huni.model.UploadPhotoModel;
import com.huni.model.UserInfoModel;
import com.huni.model.UserLoginTokenModel;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by hltognc on 21.11.2016.
 */

public interface RestInterfaceController {

    /*  register    */
    @FormUrlEncoded
    @POST("/api/Account/Register")
    Call<Void> userSignUp(@Field("Email") String email, @Field("Password")String password
            ,@Field("ConfirmPassword")String confirmpassword);


    /*  login   */
    @FormUrlEncoded
    @POST("/token")
    Call<UserLoginTokenModel> userLogIn(@Field("grant_type") String grantType, @Field("Username") String username
    , @Field("Password")String password);


    /* upload photo */
    @Multipart
    @POST("/api/Photo/UploadPhoto")
    Call<UploadPhotoModel> uploadPhoto(@Header("Authorization") String authorization,
                                       @Part MultipartBody.Part imageFile);

    //getPhotoAttributeById
    @GET("/api/Photo/GetPhotoAttributeByPhotoID")
    Call<PhotoAttributeModel> getPhotoAttributeByPhotoId(@Header("Authorization") String authorization,
                                                         @Query("photoID") int photoId);

    //GetPhotoFacesByPhotoID
    @GET("api/Photo/GetPhotoFacesByPhotoID")
    Call<PhotoFacesModel> getPhotoFacesByPhotoID(@Header("Authorization") String authorization,
                                                 @Query("photoID") int photoId);

    //GetPhotoFaceByID
    @GET("api/Photo/GetPhotoFaceByID")
    Call<Void> getPhotoFaceByID(@Header("Authorization") String authorization,
                                                 @Query("photoFaceId") int photoFaceId);


    //getPhotoByFilter
    @GET("api/Photo/GetPhotoByFilter")
    Call<ImageFilterModel> getPhotoByFilter(@Header("Authorization") String authorization,
                                            @Query("text") String text);

    //getUserInfo
    @GET("api/Account/UserInfo")
    Call<UserInfoModel> getUserInfo(@Header("Authorization") String authorization);

    //updateUserInfo
    @FormUrlEncoded
    @POST("api/Account/UpdateUserInfo")
    Call<UpdateUserInfoModel> updateUserInfo(@Header("Authorization") String authorization,
                                             @Field("Email") String email, @Field("FirstName") String firstname,
                                             @Field("LastName") String lastname,@Field("ProfilePhotoID")int profilePhotoId,
                                             @Field("CoverPhotoID") int coverPhotoId);

    //changePassword
    @FormUrlEncoded
    @POST("api/Account/ChangePassword")
    Call<Void> changePassword(@Header("Authorization") String authorization,
                                             @Field("OldPassword") String oldpassword, @Field("NewPassword") String newpassword,
                                             @Field("ConfirmPassword") String confirmpassword);

    //getFeedbackSubjects
    @GET("api/Account/GetFeedbackSubjects")
    Call<FeedbackSubjectModel> getFeedbackSubjects(@Header("Authorization") String authorization);

    //sendFeedback
    @FormUrlEncoded
    @POST("api/Account/SendFeedBack")
    Call<SendFeedbackModel> sendFeedback(@Header("Authorization") String authorization,
                                           @Field("SubjectId") int subjectid, @Field("Message") String message,
                                           @Field("PhotoID") int photoid);

    //sendFeedbackWithoutPhotoId
    @FormUrlEncoded
    @POST("api/Account/SendFeedBack")
    Call<SendFeedbackModel> sendFeedback(@Header("Authorization") String authorization,
                                         @Field("SubjectId") int subjectid, @Field("Message") String message
                                         );


}
