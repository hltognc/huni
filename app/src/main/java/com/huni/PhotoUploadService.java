package com.huni;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.huni.activity.MainActivity;
import com.huni.activity.PhotoFacesActivity;
import com.huni.model.ImageModel;
import com.huni.model.UploadPhotoModel;
import com.ipaulpro.afilechooser.utils.FileUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by hltognc on 5.12.2016.
 */

public class PhotoUploadService extends Service {

    private final String TAG = this.getClass().getName();
    private NotificationManager mNotifyManager;
    private NotificationCompat.Builder mBuilder;


    private boolean isNetworkEnabled = false;
    private boolean isWorkingNow = false;

    private int count = 0;
    private DatabaseHelper huniDB;
    private RestInterfaceController serviceAPI;
    private UploadPhotoModel uploadPhotoModel;
    //    private static final String header = "Bearer lB9sCnwGh2QxM9qNsrg5cMk931yAA54Ix_QDvt43skcqVxzYunCDMjq0JApkigqx_TZPfPaau8UFGuhtHhbt-dV4gMRBJyYYBqBv0nh-Q-wEbTXqD5_9aWXiVkXq8Me_NXp5HYM3s4Eb1_6np5cMRvb2XW7n7lvLoeCKk7aFLwn78NsGdb-sdrXEt8ux7mOMUlPJAhbaxr-imYnZGyJRP3UvUuX0ZxAqcl81ditoeRwhVsgkHPOtfqbqt2ghq6vnNo8CXbcYnK_lyqySKK6lzsHVzIEFvMGcekrvpwif-UJjw_DZL3dkFB6VZ4lIZm5h_nbXke3QPPYZCgFejDc9SHpCgS5WUkenI18cNhxbGSs36ZotKVWHZ9_zILaWQ8OzN1Vyg7bvDbROzTgFrZbgES-ISD_s5n1TSQTdseqQAsU_0V7yaPTrRJk1UZKyPsMfy81zyO_CJcWIMB-kT4PGcb63e6nnJe73_MOeaikeH6KG2e6hDCb3ZC6B6avkFxd9uvAsXgbBGoHrnZb5lv1WbQ";
    private String header = "";
    ConnectivityManager manager;
    boolean is3g,isWifi=false;

//    // Binder given to clients
//    private final IBinder mBinder = new LocalBinder();
//    // Random number generator


    //    private Context context;
    private List<ImageModel> imagesArrayList = new ArrayList<>();


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, " PhotoUploadService onStartCommand();");
        huniDB = new DatabaseHelper(this);
        huniDB.getWritableDatabase();
        manager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        try {

            imagesArrayList = intent.getParcelableArrayListExtra("images");
//            isNetworkEnabled = HuniUtil.isNetworkAvailable(context.);
//            if (isNetworkEnabled) {
            mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            mBuilder = new NotificationCompat.Builder(this);
            mBuilder.setContentTitle("Upload")
                    .setContentText("Upload in progress")
                    .setSmallIcon(R.drawable.upload_icon);
            mBuilder.setProgress(imagesArrayList.size(), 0, false);
            mNotifyManager.notify(1, mBuilder.build());

            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(5, TimeUnit.MINUTES)
                    .readTimeout(5, TimeUnit.MINUTES)
                    .writeTimeout(5, TimeUnit.MINUTES)
                    .build();


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(GeneralValues.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();

            serviceAPI = retrofit.create(RestInterfaceController.class);

            retrofitFunc(imagesArrayList);


//            } else {
//

        }
        catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "onStartCommand catch();");
        }
        return START_STICKY;
    }

    private void retrofitFunc(final List<ImageModel> imagesArrayList) {
        is3g = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
                .isConnectedOrConnecting();
        isWifi = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
                .isConnectedOrConnecting();

        if (isWifi || PreferencesHuni.getValue(GeneralValues.SETTINGS_USE_ONLY_WIFI).equals("0")){
            try {



                if (checkImagesisUploaded(imagesArrayList.get(count).getImageUri().toString())) {
                    count = count + 1;
                    Log.e(TAG, "That image has uploaded before.");
                    mBuilder.setProgress(imagesArrayList.size(), count, false);
                    mNotifyManager.notify(1, mBuilder.build());
                    Log.e(TAG, " checkImagesisUploaded count=" + count);
                    if (imagesArrayList.size() > count) {
                        retrofitFunc(imagesArrayList);
                    } else {
                        // Removes the progress bar
                        mBuilder.setContentText("Upload has completed");
                        mBuilder.setProgress(0, 0, false);
                        mNotifyManager.notify(1, mBuilder.build());
                        stopSelf();
                        return;
                    }
                }
                File file = FileUtils.getFile(PhotoUploadService.this, imagesArrayList.get(count).getImageUri());
                RequestBody fbody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                MultipartBody.Part imageFileBody = MultipartBody.Part.createFormData("image", file.getName(), fbody);
                Log.e(TAG, " Try catch öncesi count=" + count);
                try {
                    header = PreferencesHuni.getValue(GeneralValues.LOGIN_ACCESS_TOKEN);
                } catch (Exception e) {
                    Log.e(TAG, "PhotoUploadService header Exception()");
                }
                Call<UploadPhotoModel> call = serviceAPI.uploadPhoto("Bearer " + header, imageFileBody);

                Log.e(TAG, "Call öncesi count=" + count);
                call.enqueue(new Callback<UploadPhotoModel>() {
                    @Override
                    public void onResponse(Call<UploadPhotoModel> call, Response<UploadPhotoModel> response) {
                        Log.e(TAG, "onResponse ilk count=" + count);
                        int code = response.code();
                        Log.e(TAG, "response = " + response.isSuccessful());
                        if (response.isSuccessful()) {
                            Log.e(TAG, "code=" + response.code());
                            uploadPhotoModel = new UploadPhotoModel();
                            Log.e(TAG, "onResponse is successful count=" + count);
                            if (code == 200 || code == 400) {
                                uploadPhotoModel.setData(response.body().getData());
                                Log.e(TAG, uploadPhotoModel.getData());
                            } else {
                                Log.e(TAG, " code!=200 or 400");
                            }
                            huniDB.insertPhoto(Integer.parseInt(uploadPhotoModel.getData().split(":")[1]),
                                    imagesArrayList.get(count).getImageUri().toString());
                            count = count + 1;
                            mBuilder.setProgress(imagesArrayList.size(), count, false);
                            mNotifyManager.notify(1, mBuilder.build());
                            Log.e(TAG, "count , Tag " + count + "-" + imagesArrayList.size());

                            if (imagesArrayList.size() > count) {
                                Log.e(TAG, "imagesArrayList.size() > count =" + count);
                                retrofitFunc(imagesArrayList);

                            } else {
                                Log.e(TAG, "Else ilk count =" + count);
                                // Removes the progress bar
                                mBuilder.setContentText("Upload has completed");
                                mBuilder.setProgress(0, 0, false);
                                mNotifyManager.notify(1, mBuilder.build());

                                stopSelf();
                                return;
                            }

                        } else {
                            Log.e(TAG, "else 2imagesArrayList.size() > count =" + count);
                            retrofitFunc(imagesArrayList);

                        }


                    }

                    @Override
                    public void onFailure(Call<UploadPhotoModel> call, Throwable t) {
                        Log.e(TAG, " onFailure();");
                        Log.e(TAG, "Failure count =" + count);
                        if (imagesArrayList.size() > count) {
                            retrofitFunc(imagesArrayList);
                        }
                    }
                });

            }
            catch (Exception e) {
                Log.e(TAG, " onFailure();");
            }
        }

        else {
            Toast.makeText(this,"Check your connection settings for upload images.",Toast.LENGTH_LONG).show();
        }


    }

    private boolean checkImagesisUploaded(String imageUri) {
        return huniDB.checkPhotoUploadStatusByImageUri(imageUri);

    }


    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
        PhotoUploadService getService() {
            // Return this instance of LocalService so clients can call public methods
            return PhotoUploadService.this;
        }
    }


    @Nullable
    @Override
    public IBinder onBind(Intent ıntent) {
//        return mBinder;
        return null;
    }


    @Override
    public void onCreate() {
        super.onCreate();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


}
